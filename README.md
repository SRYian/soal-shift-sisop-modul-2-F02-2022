# soal-shift-sisop-modul-2-F02-2022

# Group

1. **Billy Brianto**(5025201080)
2. Amsal Herbert(5025201182)
3. Aqil Ramadhan Hadiono(5025201261)

# Dasar Penyelesaian

Pada soal nomor 1, 2, dan 3 dibutuhkan untuk membuat directory dan memindahkan file. Untuk mencapai hal tersebut digunakan command

```c
//command dideclare dalam char pointer array
int execvp (const char *file, char *const argv[]);
//dan
//command dapat langsung dimasukan
int execl(const char *path, const char *arg,.../* (char  *) NULL */);
```

Namun, masalah baru terjadi, dimana exec hanya dapat dieksekusi hanya satu kali dalam sebuah proses. Masalah tersebut dapat ditangani oleh command

```c
fork();
```

yang akan membuat proses baru.

Proses forking dibuat sekuensial dengan function sleep, dimana sleep akan mendelay masing masing fork sehingga berjalan satu per satu.
Struktur kode untuk membuat proses berjalan sekuensial adalah

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Driver code
int main()
{
    int pid, pid1, pid2;

    // variable pid will store the
    // value returned from fork() system call
    pid = fork();

    // If fork() returns zero then it
    // means it is child process.
    if (pid == 0) {

        // First child needs to be printed
        // later hence this process is made
        // to sleep for 3 seconds.
        sleep(3);

        // This is first child process
        // getpid() gives the process
        // id and getppid() gives the
        // parent id of that process.
        printf("child[1] --> pid = %d and ppid = %d\n",
               getpid(), getppid());
    }

    else {
        pid1 = fork();
        if (pid1 == 0) {
            sleep(2);
            printf("child[2] --> pid = %d and ppid = %d\n",
                   getpid(), getppid());
        }
        else {
            pid2 = fork();
            if (pid2 == 0) {
                // This is third child which is
                // needed to be printed first.
                printf("child[3] --> pid = %d and ppid = %d\n",
                       getpid(), getppid());
            }

            // If value returned from fork()
            // in not zero and >0 that means
            // this is parent process.
            else {
                // This is asked to be printed at last
                // hence made to sleep for 3 seconds.
                sleep(3);
                printf("parent --> pid = %d\n", getpid());
            }
        }
    }

    return 0;
}
```

dimana sekuens terminasi adalah

1. Anak ketiga
2. Anak kedua
3. Anak pertama
4. Parent

source: [GeeksforGeeks](https://www.geeksforgeeks.org/using-fork-produce-1-parent-3-child-processes/)

Untuk tiap nomor juga diperlukan untuk directory listing yang dapat dilakukan dengan

```c
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>


int main (void)
{
    DIR *dp;
    struct dirent *ep;
    char path[100];

    printf("Enter path to list files: ");
    scanf("%s", path);

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          //ep->d_name adalah full filepath
          puts (ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    return 0;
}
```

# Number 1

Mohon maaf, nomor satu tidak dapat diselesaikan tepat waktu

# Number 2

Untuk nomor 2, akan dibuat 3 proses menurut proses sekuensial.

Yang pertama dilakukan adalah melakukan fork pada proses. Menurut pembuatan proses sekuensial, yang pertama dieksekusi adalah brach if terdalam dan yang terakhir adalah elsenya. Dengan mengikuti sekuens tersebut

## Extract Zip dan buat folder

```c
            pid2 = fork();
            if (pid2 == 0)
            {
                sleep(1);
                printf("third");
                execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/drakor.zip", "-p", "*.png", "-d", "/home/osboxes/shift2/drakor", NULL);
            }

            else
            {
                pid3 = fork();
                if (pid3 == 0)
                {

                    execl("/bin/mkdir", "/bin/mkdir", "-p", "/home/osboxes/shift2/drakor", NULL);
                }
                else
                {
                    sleep(4);
                }
            }
```

Pada rposes fork pertama, akan dijalankan command mkdir lewat execl, kemudian naik satu tingkatan, command selanjutnya yang akan dijalankan adalah unzip. Dalam bentuk raw command dapat dinyatakan secara berikut:

```bash
unzip /home/osboxes/Downloads/drakor.zip -p *.png -d /home/osboxes/shift2/drakor
```

parameter -p menyatakan untuk hanya mengekstrak file png
parameter -d menunjukan tujuan folder

## Pemrosesan File

Untuk membuat kategori, memindahkan dan memisahkan poster, dapat diikuti alur berikut

1. split string by \_ (handle yg poster dobel)
2. split string by : dan ,
3. ambil nama kategori, buat folder untuk masing-masing kategori
4. copy file poster ke directory yg sesuai
5. delete file original

### Memisahkan poster

```c
if (strstr(ep->d_name, ".png"))
{
    // if (strstr(ep->d_name, "_"))
    // {
    char dr_name[100] = "";
    strcpy(dr_name, ep->d_name);
    int i = 0;
    char *p = strtok(dr_name, "_");
    char *array[100];
    while (p != NULL)
    {
        array[i++] = p;
        p = strtok(NULL, "_");
    }
    // printf("\nooga %s\n", ep->d_name);
    for (i = 0; i < 2; ++i)
    {
        //proses pembuatan kategori dan pemindahan
    }
}
```

Untuk memisahkan poster, dilakukan pemisahan menurut token "\_" menggunakan strtok. Kemudian hasil pemisahan akan dimasukan kedalam array. Semua ini dilakukan dlam sekali iterasi while loop, sehingga file yang akan ditarget oleh hasil pemisahan pasti ada.

### Buat Kategori

```c
//dalam for loop
if (strstr(ep->d_name, array[i]))
                            {
                                char dr_name[100] = "";
                                strcpy(dr_name, array[i]);
                                int j = 0;
                                char *p = strtok(dr_name, "; .");
                                char *ind_name[100];

                                while (p != NULL)
                                {
                                    ind_name[j++] = p;
                                    p = strtok(NULL, "; .");
                                }
                                // tokens are splitted
                                for (int j = 0; j < 4; j++)
                                {
                                    printf("%s ", ind_name[j]);
                                }
                                printf("\n");

                                // construct directory for categories
                                pid_t pid4, pid5, pid6;
                                pid4 = fork();
                                if (pid4 == 0)
                                {
                                    // sleep(2);
                                    char dir[100] = "";
                                    strcat(dir, path);
                                    strcat(dir, "/");
                                    strcat(dir, ind_name[2]);
                                    printf("\n\ndirname: %s\n\n", ind_name[2]);
                                    execl("/bin/mkdir", "/bin/mkdir", "-p", dir, NULL);
                                }
                                else
                                {
                                    //pemindahan poster
                                }
                            }

```

Untuk membuat kategori yang awalnya tidak diketahui, akan dilanjutakn dari dalam for loop untuk masing-masing nama file, nama file akan dipisahkan(lagi) menggunakan strtok dengan token ";", "," untuk mendapatkan kategori yang berada pada urutan kedua. Kemudian dilakukan fork untuk menjalankan exec membuat directory kategori tersebut.

### Memindahkan poster

```c
if (strstr(array[i], ind_name[3]))
                                    {
                                        // construct the directory
                                        // path: /home/osboxes/shift2/drakor
                                        // ep->d_name -> full original name
                                        // array -> full single name
                                        // ind_name -> single name
                                        char direc[100] = "";
                                        strcat(direc, path);
                                        strcat(direc, "/");
                                        strcat(direc, ep->d_name);
                                        printf("direction name:  %s\n", direc);

                                        // construct the filename
                                        char file_name[100] = "";
                                        strcat(file_name, path);
                                        strcat(file_name, "/");
                                        strcat(file_name, ind_name[2]);
                                        strcat(file_name, "/");
                                        strcat(file_name, ind_name[0]);
                                        strcat(file_name, ".png");

                                        printf("file name: %s\n", file_name);

                                        char filedirec[100] = "";
                                        strcat(filedirec, path);
                                        strcat(filedirec, "/");
                                        strcat(filedirec, ind_name[2]);
                                        strcat(filedirec, "/");
                                        strcat(filedirec, "data.txt");
                                        // if (fork() == 0)
                                        // {
                                        pid5 = fork();
                                        if (pid5 == 0)
                                        {
                                            /* code */
                                            sleep(4);
                                            execl("/bin/cp", "/bin/cp", "-r", direc, file_name, NULL);
                                        }
                                        else
                                        {
                                            pid6 = fork();
                                            if (pid6 == 0)
                                            {
                                                sleep(5);

                                                FILE *fptr;
                                                fptr = fopen(filedirec, "a+");
                                                fprintf(fptr, "%s\n\n", ind_name[2]);

                                                fprintf(fptr, "nama: %s", ind_name[0]);
                                                fprintf(fptr, "rilis: %s\n\n", ind_name[1]);

                                                fclose(fptr);
                                                char dir[100] = "";
                                                strcat(dir, path);
                                                strcat(dir, "/");
                                                strcat(dir, ep->d_name);
                                                printf("\n\n%s\n\n", dir);
                                                execl("/bin/rm", "/bin/rm", dir, NULL);
                                            }
                                            else
                                            {
                                                sleep(5);
                                                if (fork() == 0)
                                                {
                                                    char dir[100] = "";
                                                    /* code */ strcat(dir, path);
                                                    strcat(dir, "/");
                                                    strcat(dir, ind_name[2]);
                                                    strcat(dir, ".png");
                                                    execl("/bin/rm", "/bin/rm", dir, NULL);
                                                }
                                            }
                                        }

                                        //     exit(0);
                                        // }
                                        // else
                                        // {
                                    }
```

Dalam satu iterasi akan dibuat kateogri yang sesuai dengan poster yang dibaca, hal ini menyebabkan tiap poster dapat dipindahkan secara langsung karena folder kategori sudah ada.

Hal pertama yang dilakukan adalah memastikan apakah kategori sesuai dengan nama poster sekarang. Kemudian, tujuan file dan nama file akan dibuat menggunakan strcat. Selanjutnya, file yang sesuai akan dicopy ke kategori yang sesuai dengan kategorinya. Akhirnya, menggunakan proses forking secara sekuensial, folder dan file sisa dalam directory luar akan didelete menggunakan command rm.

# Number 3

Untuk nomor 3, akan dibuat 4 proses(tidak termasuk fork lainnya)

Yang pertama dilakukan adalah melakukan fork pada proses. Menurut pembuatan proses sekuensial, yang pertama dieksekusi adalah brach if terdalam dan yang terakhir adalah elsenya. Dengan mengikuti sekuens tersebut

## Buat directory darat dan air

```c
//fork sekuens kedua
pid3 = fork();

//cek jika tidak ada parent
if (pid3 == 0)
{
    sleep(3);
    execv(mkdir, mkdirair);
}

else
{
    //fork sekuens pertama
    pid4 = fork();
    //cek jika tidak ada parent
    if (pid4 == 0)
    {
        execv(mkdir, mkdirdarat);
    }
    else
    {
        //proses bagian e
    }
}
```

Pertama, dilakukan fork terdalam untuk menjalankan

```c
execv(mkdir, mkdirdarat);

```

lalu selanjutnya dalam proses berbeda dijalankan

```c
//tunggu 3 detik lalu exec
sleep(3);
execv(mkdir, mkdirair);

```

yang menunggu 3 detik baru dieksekusi

## Extract zip

```c
pid2 = fork();
if (pid2 == 0)
{
    sleep(4);
    execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/animal.zip", NULL);
}
else{
    //blok if else untuk membuat directory
}
```

Proses untuk extract zip mirip dengan proses membaut directory, fork proses baru, beri delay, dan lakukan execl.
Karena tidak diberi argumen untuk lokasi unzip, hasil folder unzip akan ditempatkan dalam workspace tempat file program c ini.

## Pisahkan hewan darat dan air

### Forking utama

```c
pid1=fork();
if(pid1=0)
{
    //pisahkan darat dan air
}
else{
    //blok if else extract zip
}
```

Dengan proses forking yang serupa, dapat dijalankan proses untuk memisahkan darat dan air. Proses ini menggunakan dasar directory listing.

```c
sleep(5);
// dah jalan
DIR *dp;
struct dirent *ep;
char path[100] = "/home/osboxes/Documents/Gitlab/soal-shift-sisop-modul-2-F02-2022/animal";
dp = opendir(path);
int status = 0;
if (dp != NULL)

    while ((ep = readdir(dp)) != NULL)
    {
        // puts(ep->d_name);
        // printf("%s\n", ep->d_name);
        pid_t pidin = fork();
        if (strstr(ep->d_name, "darat") && pidin == 0)
        {
            char name[30] = "./animal/";
            // printf("%s\n", ep->d_name);
            strcat(name, ep->d_name);
            execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/darat", NULL);
            // execl("/bin/echo", "/bin/echo", "hello", NULL);
            wait(&status);
        }
        else if (strstr(ep->d_name, "air") && pidin == 0)
        {
            char name[30] = "./animal/";
            // printf("%s\n", ep->d_name);
            strcat(name, ep->d_name);
            execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/air", NULL);
            // execl("/bin/echo", "/bin/echo", "hello", NULL);
            wait(&status);
        }
        else if (strstr(ep->d_name, "air") != 1 && strstr(ep->d_name, "darat") != 1 && pidin == 0)
        {
            char name[30] = "./animal/";
            strcat(name, ep->d_name);
            execl("/bin/rm", "/bin/rm", name, NULL);
        }

    (void)closedir(dp);
}
else
    perror("Couldn't open the directory");
```

Jika diperhatikan, dapat terlihat bahwa
Variabel Path menunjukan path zip yang sudah diekstrak dan
Dalam pembacaan while loop, ep->d_name menunjukan filename lengkap, dengan menggunakan informasi tersebut, dapat dilakukan filtering

### Filtering

```c
if (strstr(ep->d_name, "darat") && pidin == 0)
{
    //buat directory
    char name[30] = "./animal/";
    strcat(name, ep->d_name);
    execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/darat", NULL);
    wait(&status);
}
else if (strstr(ep->d_name, "air") && pidin == 0)
{
    //buat directory
    char name[30] = "./animal/";
    strcat(name, ep->d_name);
    execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/air", NULL);
    wait(&status);
}
else if (strstr(ep->d_name, "air") != 1 && strstr(ep->d_name, "darat") != 1 && pidin == 0)
{
    char name[30] = "./animal/";
    strcat(name, ep->d_name);
    execl("/bin/rm", "/bin/rm", name, NULL);
}
```

Masing masing blok akan mengecek jika dalam nama file terdapat substring. Kemudian path akan dibuat menggunakan strcat dan command mv(untuk move) akan dieksekusi dan status akan ditunggu.
Untuk delete hewan yang bukan darat maupun air, dilakukan pembuatan directory yang sama dan command yang akan di eksekusi adalah rm(remove).

## Bird Elimination

Menggunakan template file listing yang sama seperti sebelumnya, dapat file dalam suatu diectory dapat dilist. Klai ini, target untuk path adalah /modul2/darat

```c
char path[100] = "/home/osboxes/modul2/darat";
```

```c
while ((ep = readdir(dp)) != NULL)
{
    // puts(ep->d_name);
    // printf("%s\n", ep->d_name);
    pid_t pidin = fork();
    if (strstr(ep->d_name, "bird") && pidin == 0)
    {
        char name[50];
        strcpy(name, path);
        // printf("%s\n", ep->d_name);
        strcat(name, "/");
        strcat(name, ep->d_name);
        execl("/bin/rm", "/bin/rm", name, NULL);
        wait(&status);
    }
    else if (strstr(ep->d_name, "bird") != 1 && pidin == 0)
    {
        break;
    }
}
```

Dalam while loop, akan dibuat proses baru dimana dialkukan pengecekan jika filename memiliki string "bird", jika iya, buat filepath yang sesuai, dan lakukan remove. Jika tidak, break proses.

## List Permission

Seperti yang dinyatakan dalam bagian Buat DIrectory, blok kode list permission berada pada else paling dalam. Dalam arti lain, proses ini merupakan parent yang akan dijalankan terakhir.

```c
//untuk listing driectory
struct dirent *ep;
//untuk file
FILE *fptr;
//untuk directory
DIR *dp;
//container stat
struct passwd *p;
//id untuk forking
uid_t uid;
//struct dan integer untuk stat dan status stat
struct stat fs;
int r;
char path[100] = "/home/osboxes/modul2/air";
fptr = fopen("/home/osboxes/modul2/air/list.txt", "w");
dp = opendir(path);
if (fptr == NULL)
{
    printf("File does not exist.\n");
    return;
}
```

Untuk mendapatkan dan menulis ke file, diperlukan file pointer, untuk mendapatkan permision, diperlukan struct dan int untuk stats.

```c
if (strstr(ep->d_name, "air")){
    if ((p = getpwuid(uid = getuid())) == NULL)
    {
        perror("getpwuid() error");
    }
    else
    {
        //path building
        char statpath[100] = "";
        strcpy(statpath, path);
        strcat(statpath, "/");
        strcat(statpath, ep->d
        //ambil stats
        r = stat(statpath, &fs);
        if (r == -1)
        {
            fprintf(stderr, "File error\n");
            exit(1);
        }
        char name[100];
        strcat(name, p->pw_name);
        strcat(name, "_");
        //append masing masing chr jika memenuhi r w atau x
        if (fs.st_mode & S_IRUSR)
            strcat(name, "r");
        if (fs.st_mode & S_IWUSR)
            strcat(name, "w");
        if (fs.st_mode & S_IXUSR)
            strcat(name, "x");
        strcat(name, "_");
        strcat(name, ep->d_name);
        fprintf(fptr, "%s\n", name);
    }
}
```

Dalam while loop traversal directory, tiap file dengan susbtring air akan dicek menggunakan

```c
p = getpwuid(uid = getuid()
```

Jika hasil tidak null(jika stats file berhasil diambil), lakukan path building dengan strcat dan ambil stats dari file tersebut. Akhirnya, build nama string yang akan diprint emnggunakan strcat.

```c
if (fs.st_mode & S_IRUSR)
    strcat(name, "r");
if (fs.st_mode & S_IWUSR)
    strcat(name, "w");
if (fs.st_mode & S_IXUSR)
    strcat(name, "x");
```

Blok if tersebut akan megappend permission file(r, w, atau x) kepada filename.

# Masalah dan kendala
### Nomor 1
![nomer1](https://gitlab.com/uploads/-/system/personal_snippet/2277309/4c92136e82907f6eca2a8d4021ffa1b4/1.png)
- Forking sangat kompleks
- Kurangnya fleksibilitas compile json dalam vscode
- Sulit membagi waktu

### Nomor 2
![nomer2](https://gitlab.com/uploads/-/system/personal_snippet/2277309/3a76cdc6bb23ef1e0fd6aa5355025a40/2.jpg)

### nomor 3
![nomer2](https://gitlab.com/uploads/-/system/personal_snippet/2277309/02b8d3fa7734fe98ff271b5e0bea20c1/3.png)


