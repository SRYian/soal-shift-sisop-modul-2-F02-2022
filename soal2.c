#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>

int main(void)
{

    // execl("/bin/mkdir", "/bin/mkdir", "-p", "/home/osboxes/shift2/drakor", NULL);
    // execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/drakor.zip", "-p", "*.png", "-d", "/home/osboxes/shift2/drakor", NULL);

    pid_t pid, pid1, pid2, pid3;
    pid = fork();

    if (pid == 0)
    {
        // initial 3
        sleep(3);
        // execl("/bin/rm", "/bin/rm", "/home/osboxes/shift2/drakor/png", NULL);
    }

    else
    {
        pid1 = fork();
        if (pid1 == 0)
        {
            sleep(2);
            DIR *dp;
            struct dirent *ep;
            char path[100] = "/home/osboxes/shift2/drakor";
            dp = opendir(path);
            int status = 0;

            if (dp != NULL)
            {

                while ((ep = readdir(dp)) != NULL)
                {
                    // puts(ep->d_name);
                    // Separate with _
                    if (strstr(ep->d_name, ".png"))
                    {
                        // if (strstr(ep->d_name, "_"))
                        // {
                        char dr_name[100] = "";
                        strcpy(dr_name, ep->d_name);
                        int i = 0;
                        char *p = strtok(dr_name, "_");
                        char *array[100];

                        while (p != NULL)
                        {
                            array[i++] = p;
                            p = strtok(NULL, "_");
                        }
                        // printf("\nooga %s\n", ep->d_name);

                        for (i = 0; i < 2; ++i)
                        {
                            // printf("%s\n", array[i]);
                            if (strstr(ep->d_name, array[i]))
                            {
                                char dr_name[100] = "";
                                strcpy(dr_name, array[i]);
                                int j = 0;
                                char *p = strtok(dr_name, "; .");
                                char *ind_name[100];

                                while (p != NULL)
                                {
                                    ind_name[j++] = p;
                                    p = strtok(NULL, "; .");
                                }
                                // tokens are splitted
                                for (int j = 0; j < 4; j++)
                                {
                                    printf("%s ", ind_name[j]);
                                }
                                printf("\n");

                                // construct directory for categories
                                pid_t pid4, pid5, pid6;
                                pid4 = fork();
                                if (pid4 == 0)
                                {
                                    // sleep(2);
                                    char dir[100] = "";
                                    strcat(dir, path);
                                    strcat(dir, "/");
                                    strcat(dir, ind_name[2]);
                                    printf("\n\ndirname: %s\n\n", ind_name[2]);
                                    execl("/bin/mkdir", "/bin/mkdir", "-p", dir, NULL);
                                }
                                else
                                {

                                    if (strstr(array[i], ind_name[3]))
                                    {
                                        // construct the directory
                                        // path: /home/osboxes/shift2/drakor
                                        // ep->d_name -> full original name
                                        // array -> full single name
                                        // ind_name -> single name
                                        char direc[100] = "";
                                        strcat(direc, path);
                                        strcat(direc, "/");
                                        strcat(direc, ep->d_name);
                                        printf("direction name:  %s\n", direc);

                                        // construct the filename
                                        char file_name[100] = "";
                                        strcat(file_name, path);
                                        strcat(file_name, "/");
                                        strcat(file_name, ind_name[2]);
                                        strcat(file_name, "/");
                                        strcat(file_name, ind_name[0]);
                                        strcat(file_name, ".png");

                                        printf("file name: %s\n", file_name);

                                        char filedirec[100] = "";
                                        strcat(filedirec, path);
                                        strcat(filedirec, "/");
                                        strcat(filedirec, ind_name[2]);
                                        strcat(filedirec, "/");
                                        strcat(filedirec, "data.txt");
                                        // if (fork() == 0)
                                        // {
                                        pid5 = fork();
                                        if (pid5 == 0)
                                        {
                                            /* code */
                                            sleep(4);
                                            execl("/bin/cp", "/bin/cp", "-r", direc, file_name, NULL);
                                        }
                                        else
                                        {
                                            pid6 = fork();
                                            if (pid6 == 0)
                                            {
                                                sleep(5);

                                                FILE *fptr;
                                                fptr = fopen(filedirec, "a+");
                                                fprintf(fptr, "%s\n\n", ind_name[2]);

                                                fprintf(fptr, "nama: %s", ind_name[0]);
                                                fprintf(fptr, "rilis: %s\n\n", ind_name[1]);

                                                fclose(fptr);
                                                char dir[100] = "";
                                                strcat(dir, path);
                                                strcat(dir, "/");
                                                strcat(dir, ep->d_name);
                                                printf("\n\n%s\n\n", dir);
                                                execl("/bin/rm", "/bin/rm", dir, NULL);
                                            }
                                            else
                                            {
                                                sleep(5);
                                                if (fork() == 0)
                                                {
                                                    char dir[100] = "";
                                                    /* code */ strcat(dir, path);
                                                    strcat(dir, "/");
                                                    strcat(dir, ind_name[2]);
                                                    strcat(dir, ".png");
                                                    execl("/bin/rm", "/bin/rm", dir, NULL);
                                                }
                                            }
                                        }

                                        //     exit(0);
                                        // }
                                        // else
                                        // {
                                    }
                                }
                            }
                        }
                        // }
                    }

                    // if (fork() == 0)
                    // {
                    // sleep(0.7);
                    // char dir[100] = "";
                    // strcat(dir, path);
                    // strcat(dir, "/");
                    // strcat(dir, ep->d_name);
                    // printf("\n\n%s\n\n", dir);
                    // execl("/bin/rm", "/bin/rm", dir, NULL);
                    // exit(0);
                    // }
                }
                // commit die
                (void)closedir(dp);
            }
            else
            {
                perror("Couldn't open the directory");
            }
        }
        else
        {
            pid2 = fork();
            if (pid2 == 0)
            {
                sleep(1);
                printf("third");
                execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/drakor.zip", "-p", "*.png", "-d", "/home/osboxes/shift2/drakor", NULL);
            }

            else
            {
                pid3 = fork();
                if (pid3 == 0)
                {

                    execl("/bin/mkdir", "/bin/mkdir", "-p", "/home/osboxes/shift2/drakor", NULL);
                }
                else
                {
                    sleep(4);
                }
            }
        }
    }
    // char drakor_ct[7][20] = {"thriller", "horror", "action", "comedy", "romance", "school", "fantasy"};

    // for (int i = 0; i < 7; i++) // loop will run n times (n=5)
    // {
    //     if (fork() == 0)
    //     {
    //         char dir[100] = "/home/osboxes/shift2/drakor/";
    //         strcat(dir, drakor_ct[i]);
    //         execl("/bin/mkdir", "/bin/mkdir", "-p", dir, NULL);
    //         exit(0);
    //     }
    // }
    // for (int i = 0; i < 5; i++) // loop will run n times (n=5)
    //     wait(NULL);
    // printf("\ndandas\n");

    return 0;
}