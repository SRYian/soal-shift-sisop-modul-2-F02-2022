#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>

int main(void)
{
    char *mkdir = "/bin/mkdir";
    char *mkdirhere[] = {mkdir, "-p", "./animals", NULL};
    char *mkdirdarat[] = {mkdir, "-p", "/home/osboxes/modul2/darat", NULL};
    char *mkdirair[] = {mkdir, "-p", "/home/osboxes/modul2/air", NULL};

    pid_t pid, pid1, pid2, pid3, pid4;
    pid = fork();

    if (pid == 0)
    {
        sleep(6);
        DIR *dp;
        struct dirent *ep;
        char path[100] = "/home/osboxes/modul2/darat";

        dp = opendir(path);
        int status = 0;
        if (dp != NULL)
        {
            while ((ep = readdir(dp)) != NULL)
            {
                // puts(ep->d_name);
                // printf("%s\n", ep->d_name);
                pid_t pidin = fork();

                if (strstr(ep->d_name, "bird") && pidin == 0)
                {
                    char name[50];
                    strcpy(name, path);
                    // printf("%s\n", ep->d_name);
                    strcat(name, "/");
                    strcat(name, ep->d_name);
                    execl("/bin/rm", "/bin/rm", name, NULL);
                    wait(&status);
                }
                else if (strstr(ep->d_name, "bird") != 1 && pidin == 0)
                {
                    break;
                }
            }

            (void)closedir(dp);
        }
        else
            perror("Couldn't open the directory");
    }

    else
    {
        pid1 = fork();
        if (pid1 == 0)
        {
            sleep(5);
            // dah jalan
            DIR *dp;
            struct dirent *ep;
            char path[100] = "/home/osboxes/Documents/Gitlab/soal-shift-sisop-modul-2-F02-2022/animal";
            dp = opendir(path);
            int status = 0;
            if (dp != NULL)
            {

                while ((ep = readdir(dp)) != NULL)
                {
                    // puts(ep->d_name);
                    // printf("%s\n", ep->d_name);
                    pid_t pidin = fork();

                    if (strstr(ep->d_name, "darat") && pidin == 0)
                    {
                        char name[30] = "./animal/";
                        // printf("%s\n", ep->d_name);
                        strcat(name, ep->d_name);
                        execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/darat", NULL);
                        // execl("/bin/echo", "/bin/echo", "hello", NULL);
                        wait(&status);
                    }
                    else if (strstr(ep->d_name, "air") && pidin == 0)
                    {
                        char name[30] = "./animal/";
                        // printf("%s\n", ep->d_name);
                        strcat(name, ep->d_name);
                        execl("/bin/mv", "/bin/mv", name, "/home/osboxes/modul2/air", NULL);
                        // execl("/bin/echo", "/bin/echo", "hello", NULL);
                        wait(&status);
                    }
                    else if (strstr(ep->d_name, "air") != 1 && strstr(ep->d_name, "darat") != 1 && pidin == 0)
                    {
                        char name[30] = "./animal/";
                        strcat(name, ep->d_name);
                        execl("/bin/rm", "/bin/rm", name, NULL);
                    }
                }

                (void)closedir(dp);
            }
            else
                perror("Couldn't open the directory");
        }
        else
        {
            pid2 = fork();
            if (pid2 == 0)
            {
                sleep(4);
                execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/animal.zip", NULL);
            }

            else
            {
                pid3 = fork();
                if (pid3 == 0)
                {
                    sleep(3);
                    execv(mkdir, mkdirair);
                }
                else
                {
                    pid4 = fork();
                    if (pid4 == 0)
                    {
                        execv(mkdir, mkdirdarat);
                    }
                    else
                    {
                        sleep(7);
                        struct dirent *ep;
                        FILE *fptr;
                        DIR *dp;

                        struct passwd *p;
                        uid_t uid;

                        struct stat fs;
                        int r;

                        char path[100] = "/home/osboxes/modul2/air";
                        fptr = fopen("/home/osboxes/modul2/air/list.txt", "w");
                        dp = opendir(path);
                        if (fptr == NULL)
                        {
                            printf("File does not exist.\n");
                            return;
                        }
                        // fprintf(fptr, "dadad\n");
                        if (dp != NULL)
                        {

                            while ((ep = readdir(dp)) != NULL)
                            {
                                if (strstr(ep->d_name, "air"))
                                {

                                    if ((p = getpwuid(uid = getuid())) == NULL)
                                    {
                                        perror("getpwuid() error");
                                    }
                                    else
                                    {
                                        char statpath[100] = "";
                                        strcpy(statpath, path);
                                        strcat(statpath, "/");
                                        strcat(statpath, ep->d_name);

                                        r = stat(statpath, &fs);
                                        if (r == -1)
                                        {
                                            fprintf(stderr, "File error\n");
                                            exit(1);
                                        }
                                        char name[100] = "";

                                        strcat(name, p->pw_name);
                                        strcat(name, "_");
                                        if (fs.st_mode & S_IRUSR)
                                            strcat(name, "r");
                                        if (fs.st_mode & S_IWUSR)
                                            strcat(name, "w");
                                        if (fs.st_mode & S_IXUSR)
                                            strcat(name, "x");
                                        strcat(name, "_");
                                        strcat(name, ep->d_name);
                                        fprintf(fptr, "%s\n", name);
                                    }
                                }
                            }

                            (void)closedir(dp);
                        }
                        else
                            perror("Couldn't open the directory");

                        fclose(fptr);
                    }
                }
            }
        }
    }

    return 0;
}